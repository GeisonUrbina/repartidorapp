import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-side-menu',
  templateUrl: 'side-menu.html',
})
export class SideMenuPage {

  @ViewChild('content') nav: NavController;
  pages:Array<{title:string,icono:string,component:string,openTab?: any}>;
  rootPage = 'MenuPage'
  constructor(public navCtrl: NavController, public navParams: NavParams,private app:App,private storage: Storage) {
    this.pages = [
      {title:'Inicio',icono:'home',component:'MenuPage'},
      {title:'Pedidos',icono:'basket',component:'OrderPage'},
      {title:'Configuración',icono:'settings',component:'ConfigurationPage'},
      {title:'Cerrar Sesion',icono:'exit',component:'HomePage'}
    ];
  }

  abrirPagina(page){
    if(page.component === 'HomePage'){
      this.storage.remove("user_id");
      this.storage.remove("user_name");
      this.nav.setRoot(page.component,{openTab:page.openTab});
      
    }else{
      this.nav.setRoot(page.component,{openTab:page.openTab});
    }
  }
}
