import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-send-message',
  templateUrl: 'send-message.html',
})
export class SendMessagePage {

  public client_phone:any;
  public message:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private sms: SMS,private provider: GeneralQueryProvider) {
    this.client_phone = this.navParams.get('client_phone'); 
  }

  enviarMensaje(){
    this.sms.send(this.client_phone,this.message);
    this.provider.presentToast('Mensaje enviado exitosamente');
    this.navCtrl.pop();
  }
}
