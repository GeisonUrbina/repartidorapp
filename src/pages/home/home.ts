import { Component } from '@angular/core';
import { IonicPage,NavController } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[GeneralQueryProvider]
})
export class HomePage {

  phone: AbstractControl;
  loginForm: FormGroup;
  mensajeError: String;
  //Facebook
  userData:any ={};
  isUserLoggedIn:any = false;
  constructor(public navCtrl: NavController,public userSignup: GeneralQueryProvider,
    private formBuilder: FormBuilder,private storage: Storage) {
    this.loginForm = this.formBuilder.group({
      phone: ['', [Validators.required,Validators.maxLength(8),Validators.minLength(8)]]
    });
  }

  signin_request(value: any): void { 
        if(this.loginForm.valid) {
          this.mensajeError = "";
          this.userData.client_phone = value.phone;
          this.verificarExistencia("T");
        }else{
          this.mensajeError = "El número celular está vacío o es inválido";
        }
  }

  verificarExistencia(tipoLogin){
    switch(tipoLogin){
      case "T":
        this.userSignup.estaRegistrado(tipoLogin,this.userData).subscribe(Resultado =>{
          if(Resultado["status_response"] == "CREADO"){
            let usuario = Resultado['user'];
            this.userSignup.obtenerToken("84520006").subscribe(Telefono => {
              this.userData.smsToken = Telefono["token_sms"];
              this.userData.phone = usuario.delivery_person_phone;
              this.userData.name = usuario.delivery_person_name;
              this.userData.id = usuario.id;
              this.userData.action="LOGIN";
              this.navCtrl.push("PhoneVerificationPage",{userData:this.userData});
          });
          }else{
            this.mensajeError = "El número celular está vacío o es inválido";
          }
        });
      break;
    }
  }


}
