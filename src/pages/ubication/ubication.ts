import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,AlertController } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  Marker
} from '@ionic-native/google-maps';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';


declare var google;

@IonicPage()
@Component({
  selector: 'page-ubication',
  templateUrl: 'ubication.html',
})
export class UbicationPage {

  map: GoogleMap;
  lat:any;
  lon:any;
  latitud:any;
  longitud:any;
  isMapLoaded:boolean;
  //map: GoogleMap;
  constructor(private platform:Platform,public navCtrl: NavController,private alertCtrl: AlertController, public navParams: NavParams,
    private geolocation:Geolocation) {
      this.lat = this.navParams.get("lat");
      this.lon = this.navParams.get("lon");
      this.platform.ready().then(() =>{
        /*let options = {
          timeout:10000,
          enableHighAccuracy:true
        };*/
        this.geolocation.getCurrentPosition().then((pos) =>{
          this.latitud = pos.coords.latitude;
          this.longitud = pos.coords.longitude;
          this.loadMap();
          this.isMapLoaded=true;
        }).catch(err => this.presentAlert(err.message));
  
    });
  }

  presentAlert(err) {
    let alert = this.alertCtrl.create({
      title: 'Low battery',
      subTitle: this.latitud + 'prueba' + err,
      buttons: ['Dismiss']
    });
    alert.present();
  }
  

  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: this.latitud,
           lng: this.longitud
         },
         zoom: 18,
         tilt: 30
       }
       ,
       mapTypeId: 'roadmap'
    };

      this.map = GoogleMaps.create('map_canvas', mapOptions);

    let marker: Marker = this.map.addMarkerSync({
      title: 'Ubicacion actual',
      icon: '#00838f',
      animation: 'DROP',
      draggable:false,
      position: {
        lat: this.latitud,
        lng: this.longitud
      }
    });

    let marker2: Marker = this.map.addMarkerSync({
      title: 'Lugar de destino',
      icon: '#b71c1c',
      animation: 'DROP',
      draggable:false,
      position: {
        lat: this.lat,
        lng: this.lon
      }
    });


}

  volver(){
    this.navCtrl.pop();
  }
}
