import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-phone-verification',
  templateUrl: 'phone-verification.html',
})
export class PhoneVerificationPage {

  phone_code:AbstractControl;
  signupForm: FormGroup;
  userData: any = {};
  cantidadIntentos:number = 3;
  mensajeIntentos:string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams,public userSignup: GeneralQueryProvider,
    private formBuilder: FormBuilder,private storage:Storage) {
    this.userData = this.navParams.get('userData');

    this.signupForm = this.formBuilder.group({
      phone_code: ['', [Validators.required,Validators.minLength(6),Validators.maxLength(6)]]
    });
  }

  combrobarToken(value: any): void{
    if(this.signupForm.valid) {
      if(value.phone_code == this.userData.smsToken){
        //this.storage.get("user_device").then((val)=>{});
        if(this.userData.action === "LOGIN"){
          this.ingresarPlataforma();
        }
      }else{
        this.cantidadIntentos -= 1;
        this.mostrarMensajeIntentos();
      }
    }
  }

  mostrarMensajeIntentos(){
    if(this.cantidadIntentos === 0){
      this.navCtrl.pop();
    }else{
      this.cantidadIntentos === 1 ? this.mensajeIntentos = "Usted tiene " + this.cantidadIntentos + " intento restante.": this.mensajeIntentos = "Usted tiene " + this.cantidadIntentos + " intentos restantes.";
    }
  }

  ingresarPlataforma(){
    this.storage.get("user_device").then((val)=>{
      this.userSignup.actualizarDevice(this.userData.id,val).subscribe(Resultado => {
        this.storage.set("user_id",this.userData.id);
        this.storage.set("user_name",this.userData.name);
        this.navCtrl.setRoot("SideMenuPage");
      });
    });
}

}
