import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';


@IonicPage()
@Component({
  selector: 'page-end-order',
  templateUrl: 'end-order.html',
})
export class EndOrderPage {

  public codigo:any;
  public cliente:any;
  public carrito:any;
  public puntos:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private provider: GeneralQueryProvider) {
    this.codigo = this.navParams.get('codigo');
    this.cliente = this.navParams.get('cliente');
    this.carrito = this.navParams.get('carrito');
    this.puntos = 0;
  }

  finalizarCompra(){
    this.provider.finalizarPedido(this.codigo,this.cliente,this.puntos,this.carrito).subscribe(Resultado =>{
      this.navCtrl.setRoot('MenuPage');
    });
  }
}
