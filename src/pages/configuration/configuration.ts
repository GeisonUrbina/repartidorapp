import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController  } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {

  userData:any;
  user_name: string;
  user_surname: string;
  user_email: string;
  user_phone: string;
  configurationForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController,
    public user: GeneralQueryProvider,private formBuilder: FormBuilder) {
      
      this.user.getUserId().then((id)=>{
        this.user.datosRepartidor(id).subscribe(Data => {
          this.userData = Data['data'];
          this.user_name = this.userData.delivery_person_name;
          this.user_surname = this.userData.delivery_person_lastName;
          this.user_email = this.userData.delivery_person_email;
          this.user_phone = this.userData.delivery_person_phone;
        });
      });
      
      /*if(this.userData != null){
        this.user_name = this.userData.client_name;
        this.user_surname = this.userData.client_lastName;
        this.user_email = this.userData.email;
        this.user_phone = this.userData.client_phone;

        this.configurationForm = this.formBuilder.group({
          user_name: ['', Validators.required],
          user_surname: ['', Validators.required],
          user_email: ['', Validators.required],
          user_phone: ['', Validators.required],
        });
      }*/
  }

  ionViewDidLoad() {

  }

  modificarNombre() {
    let alert = this.alertCtrl.create({
      title: 'Modificar nombre',
      inputs: [
        {
          name: 'nombre',
          placeholder: 'Geison'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Modificar',
          handler: data => {
            console.log('login clicked');
          }
        }
      ]
    });
    alert.present();
  }

  modificarApellido(){
    let alert = this.alertCtrl.create({
      title: 'Modificar apellido',
      inputs: [
        {
          name: 'apellido',
          placeholder: 'urval'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Modificar',
          handler: data => {
            console.log('login clicked');
          }
        }
      ]
    });
    alert.present();
  }

  modificarTelefono(){
    let alert = this.alertCtrl.create({
      title: 'Modificar teléfono',
      inputs: [
        {
          name: 'telefono',
          placeholder: '84520006'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Siguiente',
          handler: data => {
            console.log('login clicked');
          }
        }
      ]
    });
    alert.present();
  }

  modificarEmail(){
    let alert = this.alertCtrl.create({
      title: 'Modificar correo electrónico',
      inputs: [
        {
          name: 'correo',
          placeholder: 'geisonurval@gmail.com'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Modificar',
          handler: data => {
            console.log('login clicked');
          }
        }
      ]
    });
    alert.present();
  }

  configurationForm_request(value:any):void{
    alert();
  }

}
