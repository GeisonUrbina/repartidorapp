import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { CallNumber } from '@ionic-native/call-number';
import { PopoverController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-shopping-detail',
  templateUrl: 'shopping-detail.html',
})
export class ShoppingDetailPage {

  public products:any;
  public pedido:any;
  public cliente:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public orders: GeneralQueryProvider,
    private callNumber: CallNumber,public popoverCtrl: PopoverController,public alertController: AlertController) {
    this.products = [];
    this.products = this.navParams.get('productos');
    this.pedido = this.navParams.get('pedido');
  }

  cancelarCompra(codigoCompra,idClient){
    this.orders.presentLoading();
    this.orders.cancelOrder(codigoCompra,idClient).subscribe(Resultado =>{
      
    });
    this.orders.dismissLoading();
    this.navCtrl.setRoot('OrderPage');
  }

  llamarCliente(){
    this.callNumber.callNumber(this.pedido.client_phone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  enviarMensaje(){
    this.navCtrl.push("SendMessagePage",{client_phone:this.pedido.client_phone});
  }

  verMapa(latitud,longitud){
    this.navCtrl.push("UbicationPage",{lat:latitud,lon:longitud});
  }

  finalizarPedido(){
    this.navCtrl.push("EndOrderPage",{codigo:this.pedido.codigo,cliente:this.pedido.client_id,carrito:this.pedido.carrito});
  }

  notificarLLegada(idClient){
    this.orders.presentLoading();
    this.orders.notificarLlegada(idClient).subscribe(Resultado =>{});
    this.orders.dismissLoading();
  }

  async presentAlertConfirm(codigoCompra,idClient) {
    const alert = await this.alertController.create({
      message: '<strong>¿Deseas cancelar la compra?</strong>',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'botonCancelar',
          handler: (blah) => {
          }
        }, {
          text: 'SI',
          cssClass: 'botonAceptar',
          handler: () => {
            this.cancelarCompra(codigoCompra,idClient);
          }
        }
      ]
    });

    await alert.present();
  }

}
