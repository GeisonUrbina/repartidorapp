import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';


@IonicPage()
@Component({
  selector: 'page-receipt',
  templateUrl: 'receipt.html',
})
export class ReceiptPage {
  private recibo:any;
  public montoTotal:number;
  public montoPago:any;
  public OMI:any;
  public montoPagoIngresado:any;
  public formaPago:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public provider:GeneralQueryProvider) {
    this.recibo =[];
    this.montoTotal=0;
    this.OMI = false;
    this.formaPago="EF";
  }

  ionViewDidLoad() {

    let productosGuardados ="";
    let arreglo = [];
    if(window.localStorage.getItem("cart")){
      arreglo = JSON.parse(window.localStorage.getItem("cart"));
      productosGuardados = JSON.stringify(arreglo);
    } 
    this.provider.getReceipt(productosGuardados).subscribe(Productos => {
      Productos["productos"].map(Product => {
        this.recibo.push(Product);
      });
      this.montoTotal =Productos["montoTotal"];
    });

}

ocultarInput(){
  this.montoPagoIngresado = this.montoTotal;
  this.OMI=false;
}

mostrarInput(){
  this.montoPagoIngresado = null;
  this.OMI=true;
}

  geolocation_map(){
    this.navCtrl.push("UbicationPage",{montoPagoIngresado:this.montoPagoIngresado});
  }

}
