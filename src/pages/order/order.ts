import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { EnumProvider } from '../../providers/enum/enum';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  public myOrders:any;
  public mensaje:any;
  public productos:any;
  public urlIP:String;
  constructor(public http: Http,public navCtrl: NavController, public navParams: NavParams,
    public orders: GeneralQueryProvider,public enumerados:EnumProvider,private storage:Storage) {
    this.myOrders = [];
    this.productos = [];
    this.urlIP = 'www.urbidcr.com';
  }

  ionViewDidLoad() {
    this.orders.presentLoading();
    this.storage.get("user_id").then((val)=>{
      let x = new FormData();
      x.append('user_id',val);
      this.http.post("http://"+this.urlIP+"/administration/order/deliveryOrderList/",x).map(res=>res.json()).subscribe(Pedidos => {
        Pedidos.map(Pedido =>{
          Pedido.status = this.enumerados.enumStatus(Pedido.status);
          Pedido.forma_pago = this.enumerados.enumPaymentType(Pedido.forma_pago);
          this.myOrders.push(Pedido);
        });
      });
    });
    this.orders.dismissLoading();
  }

  verProductosCompra(Pedido){
    this.productos = [];
    this.orders.getCart(Pedido.carrito).subscribe(Prots => {
      Prots.map(p => {
        this.productos.push(p);
      });
    });
    
    this.navCtrl.push("ShoppingDetailPage",{productos:this.productos,pedido:Pedido});
  }
}
