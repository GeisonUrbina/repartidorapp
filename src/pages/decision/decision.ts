import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { SideMenuPage } from '../side-menu/side-menu';

@IonicPage()
@Component({
  selector: 'page-decision',
  templateUrl: 'decision.html',
})
export class DecisionPage {
  @ViewChild('content') nav: NavController;
  rootPage:any = HomePage;
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
    this.checkPreviousAuthorization();
  }

  checkPreviousAuthorization(): void { 
    this.storage.get('user_id').then((val)=>{
      if(val === "undefined" || val === null){
          this.rootPage = HomePage;
      } else {
          this.rootPage = SideMenuPage;
      }
      this.navCtrl.setRoot(this.rootPage);
    });
  }

}
