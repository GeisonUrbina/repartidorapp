import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { EnumProvider } from '../../providers/enum/enum';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  userName:any;
  userData:any;
  public myOrders:any;
  public urlIP:String;
  public productos:any;
  constructor(public http: Http,public navCtrl: NavController, 
    public navParams: NavParams,private storage: Storage,public enumerados:EnumProvider,
    public providerGeneral: GeneralQueryProvider) {
      this.myOrders = [];
      this.productos = [];
      this.urlIP = 'www.urbidcr.com';
      this.storage.get('user_name').then((val)=>{this.userName = val;});
    
  }

  ionViewDidLoad() {
    this.providerGeneral.presentLoading();
    this.storage.get("user_id").then((val)=>{
      let x = new FormData();
      x.append('user_id',val);
      this.http.post("http://"+this.urlIP+"/administration/order/deliveryOrderList/",x).map(res=>res.json()).subscribe(Pedidos => {
        Pedidos.map(Pedido =>{
          Pedido.status = this.enumerados.enumStatus(Pedido.status);
          Pedido.forma_pago = this.enumerados.enumPaymentType(Pedido.forma_pago);
          this.myOrders.push(Pedido);
        });
      });
    });
    this.providerGeneral.dismissLoading();
  }

  verProductosCompra(Pedido){
    this.productos = [];
    this.providerGeneral.getCart(Pedido.carrito).subscribe(Prots => {
      Prots.map(p => {
        this.productos.push(p);
      });
      this.navCtrl.push("ShoppingDetailPage",{productos:this.productos,pedido:Pedido});
    });
  }
  

}
