import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class EnumProvider {

  constructor(public http: Http) {
    
  }

  enumStatus(estado){
    switch(estado){
      case "PC":
        return "Procesando compra";
      case "PE":
        return "Pendiente entrega";
      case "CE":
        return "Compra entregada";
      case "CC":
        return "Compra cancelada";
      default:
        return "Estado invalido";
    }
  }

  enumPaymentType(formaPago){
    switch(formaPago){
      case "E":
        return "Efectivo";
      case "P":
        return "Puntos URBID";
      case "T":
        return "Tarjeta";
      default:
        return "Forma de pago inválido";
    }
  }

  enumProjectStatus(estado){
    switch(estado){
      case "A":
        return "Activo";
      case "B":
        return "Cerrado";
      default:
        return "Estado invalido";
    }
  }

}
