import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import {Storage} from '@ionic/storage';


@Injectable()
export class GeneralQueryProvider {

	public urlCategorias:string;
  public urlSubcategorias:string;
  public urlProductos:string;
  public urluserLoginAPI:string;
  public urluserSignupAPI:string;
  public urlcart:string;
  public urlreceipt:string;
  public urlGenerateShopping:string;
  public urlProjects:string
  public urlIP:string;
  public loader:any;
  public createAPI:string;
  public facebookAPI:string;
  public token:string;
  private id:string;

  constructor(public http: Http,public loadingCtrl: LoadingController,private toastCtrl: ToastController,
    private storage:Storage) {

    this.urlCategorias='/categoriasAPI';
    this.urlSubcategorias='/subcategoriasAPI';
    this.urlProductos='/productosAPI';
    this.urluserLoginAPI='/userLoginAPI';
    this.urluserSignupAPI='/userSignupAPI';
    this.urlcart='/cartAPI';
    this.urlreceipt='/receiptAPI';
    this.urlGenerateShopping='/generateShoppingAPI';
    this.urlProjects='/projectsAPI';
    this.createAPI="/createAPI";
    this.facebookAPI="/facebookAPI";
    this.token="/tokenAPI";
    this.urlIP = 'www.urbidcr.com';
  }

  getOrderList(){
    this.storage.get("user_id").then((val)=>{this.id = val});
    let x = new FormData();
    x.append('correo_usuario',this.id);
    return this.http.post("http://"+this.urlIP+"/administration/order/orderList/",x).map(res=>res.json());
  }

  getProjects(){
  	return this.http.get("http://"+this.urlIP+"/app/project/projects").map(res => res.json());
  }


  getCategories(){
  	return this.http.get("http://"+this.urlIP+"/administration/category/categories").map(res => res.json());
  }

  getSubCategories(category_code){
    return this.http.get("http://"+this.urlIP+"/administration/subcategory/subcategories"+"/" + category_code).map(res => res.json());
  }

  getProducts(subcategory_code){
    return this.http.get("http://"+this.urlIP+"/administration/product/products"+"/" + subcategory_code).map(res => res.json());
  }

  getLogin(user,pass){
    let loginForm = new FormData();
    loginForm.append('client_email',user);
    loginForm.append('client_password',pass);
    return this.http.post(this.urluserLoginAPI,loginForm).map(res => res.json());
  }

  postSignup(user_name,user_surname,user_email,user_phone,user_password,create_type,user_id,user_device){
    let signupForm = new FormData();
    signupForm.append('user_name',user_name);
    signupForm.append('user_surname',user_surname);
    signupForm.append('user_email',user_email);
    signupForm.append('user_phone',user_phone);
    signupForm.append('user_password',user_password);
    signupForm.append('create_type',create_type);
    signupForm.append('user_id',user_id);
    signupForm.append('user_device',user_device);
    return this.http.post("http://www.urbidcr.com/administration/client/new-client-app/",signupForm).map(res => res.json());
  }

  getCart(cart_products){
    let cartForm = new FormData();
    cartForm.append('cart_products',cart_products);

    return this.http.post("http://www.urbidcr.com/administration/cart/cart-products/",cartForm).map(res => res.json());

  }
  
  getReceipt(cart_products){
    let cartForm = new FormData();
    cartForm.append('cart_products',cart_products);

    return this.http.post("http://"+this.urlIP+"/administration/receipt/receipt-list/",cartForm).map(res => res.json());

  }

  saveProductsInsideCart(codigo_producto, cantidad){
    let productosGuardados = [];
    if(window.localStorage.getItem("cart")) productosGuardados = JSON.parse(window.localStorage.getItem("cart"));
    productosGuardados.push({"codigo":codigo_producto,"cantidad":cantidad});
    
    window.localStorage.setItem("cart",JSON.stringify(productosGuardados));
  }

  deleteProductInsideCart(codigo_producto){
    let productosGuardados = [];
    if(window.localStorage.getItem("cart")){
      productosGuardados = JSON.parse(window.localStorage.getItem("cart"));
      
      for(let i=0;i<productosGuardados.length;i++){
        if(productosGuardados[i].codigo === codigo_producto){
            productosGuardados.splice(i,1);
            break;
        }
      }
      window.localStorage.setItem("cart",JSON.stringify(productosGuardados));
    } 
  }

  saveProductInFavorite(codigo_producto,cantidad){
    let productosGuardados = [];
    return new Promise((resolve, reject) => {
      this.storage.get("favorite").then(val => {
        productosGuardados = JSON.parse(val);
        productosGuardados.push({"codigo":codigo_producto,"cantidad":1});
        this.storage.set("favorite",JSON.stringify(productosGuardados));
        resolve();
      }).catch(err => {
        this.storage.set("favorite",JSON.stringify(productosGuardados));
        reject();
      });

    });
  }

  deleteProductInFavorite(codigo_producto){
    let productosGuardados = [];
    return new Promise((resolve,reject)  => {
      this.storage.get("favorite").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].codigo === codigo_producto){
              productosGuardados.splice(i,1);
              break;
          }
        }
        this.storage.set("favorite",JSON.stringify(productosGuardados)).then(val => {
          resolve();
        }).catch(err => {
          reject();
        });
      }).catch(err => {
        reject();
      });

    });   
  }

  modifyProductCartQuantity(codigo_producto,cantidad){
    let productosGuardados = [];
    if(window.localStorage.getItem("cart")){
      productosGuardados = JSON.parse(window.localStorage.getItem("cart"));
      
      for(let i=0;i<productosGuardados.length;i++){
        if(productosGuardados[i].codigo === codigo_producto){
          productosGuardados[i].cantidad = cantidad;
            break;
        }
      }
      window.localStorage.setItem("cart",JSON.stringify(productosGuardados));
    } 
  }

  generarCompra(){
    let productosGuardados = this.obtenerProductosCarritosLocal();
    let cartForm = new FormData();
    cartForm.append('cart_products',productosGuardados);

    return this.http.post("http://"+this.urlIP+"/administration/receipt/receipt-list/",cartForm).map(res => res.json());
  }

  procesarCompra(latitud,longitud,montoPagoIngresado){
    let productosGuardados = this.obtenerProductosCarritosLocal();
    let cartForm = new FormData();
    cartForm.append('cart_products',productosGuardados);
    cartForm.append('position_latitud',latitud);
    cartForm.append('position_longitud',longitud);
    cartForm.append('monto_pago_ingresado',montoPagoIngresado);
    return this.http.post("http://"+this.urlIP+"/administration/shopping/order/processing",cartForm).map(res => res.json());
  }

  obtenerProductosCarritosLocal(){
    let productosGuardados ="";
    let arreglo = [];
    if(window.localStorage.getItem("cart")){
      arreglo = JSON.parse(window.localStorage.getItem("cart"));
      productosGuardados = JSON.stringify(arreglo);
    } 

    return productosGuardados;
  }

  isProductFavorite(codigo_producto){
    let productosGuardados = [];
    return new Promise((resolve, reject) => {
      this.storage.get("favorite").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].codigo === codigo_producto){
              resolve(true);
          }
        }
        reject(false);
      }).catch(err => {
        reject(false);
      });
    });
  }

  isProductInCart(codigo_producto){
    let productosGuardados = [];
    if(window.localStorage.getItem("cart")){
      productosGuardados = JSON.parse(window.localStorage.getItem("cart"));
      
      for(let i=0;i<productosGuardados.length;i++){
        if(productosGuardados[i].codigo === codigo_producto){
            return true;
        }
      }
      return false;
    } 
   return false;
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismiss();
  }

  presentToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 1700,
      position: 'bottom',
      cssClass: 'urbidToast'
    });
    toast.present();
  }


  cancelOrder(codigoCompra,idCliente){
    let orderForm = new FormData();
    orderForm.append('orderCode',codigoCompra);
    orderForm.append('id_cliente',idCliente);
    return this.http.post("http://"+this.urlIP+"/administration/order/cancel",orderForm).map(res => res.json());
  }

  notificarLlegada(idCliente){
    let orderForm = new FormData();
    orderForm.append('id_cliente',idCliente);
    return this.http.post("http://"+this.urlIP+"/administration/order/notify/arrival",orderForm).map(res => res.json());
  }

  modificarAfiliacion(codigoProyecto){
    let modificar = new FormData();
    this.storage.get("user_id").then((val)=>{this.id = val});
    modificar.append('id_usuario',this.id);
    modificar.append('project_code',codigoProyecto);
   
    return this.http.post("http://"+this.urlIP+"/app/projects/affiliations",modificar).map(res => res.json());
  }

  desafiliarProyecto(){
    let desafiliar = new FormData();
    this.storage.get("user_id").then((val)=>{this.id = val}) ;
    desafiliar.append('id_usuario',this.id);

    return this.http.post("http://"+this.urlIP+"/app/projects/desaffiliation",desafiliar).map(res => res.json());
  }

  estaAfiliado(codigoProyecto){
    let esAfiliado = new FormData();
    this.storage.get("user_id").then((val)=>{this.id = val}) ;
    esAfiliado.append('id_usuario',this.id);
    esAfiliado.append('project_code',codigoProyecto);
    
    return this.http.post("http://"+this.urlIP+"/app/projects/isAffiliate",esAfiliado).map(res => res.json());
  }

  estaRegistrado(tipoLogin,userData){
    let usuario = new FormData();
    usuario.append('tipo_login',tipoLogin);
    usuario.append('client_phone',userData.client_phone);
    return this.http.post("http://"+this.urlIP+"/app/validate/deliveryPerson",usuario).map(res => res.json());
  }

  obtenerToken(number){
    let telefono = new FormData();
    telefono.append('number',number);
    return this.http.post("http://"+this.urlIP+"/app/phoneCode",telefono).map(res => res.json());

  }

  datosRepartidor(userId:string){
    let repartidor = new FormData();
    repartidor.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/datosRepartidor",repartidor).map(res => res.json());
  }

  getUserId(){
    return this.storage.get("user_id");
  }

  actualizarDispositivo(userId){
      this.storage.get('user_device').then(device => {
        let dispositivo = new FormData();
        dispositivo.append('dispositivo',device);
        dispositivo.append('id_usuario',userId);
        this.http.post("http://"+this.urlIP+"/app/updateDevice",dispositivo).map(res => res.json());
      }).catch(val => {
        alert('Not found');
      });
  }

  finalizarPedido(codigo,cliente,puntos,carrito){
    let order = new FormData();
    order.append('codigo',codigo);
    order.append('cliente',cliente);
    order.append('puntos',puntos);
    order.append('carrito',carrito);
    return this.http.post("http://"+this.urlIP+"/app/finish/order",order).map(res => res.json());
  }

  actualizarDevice(userId,device){
    let cliente = new FormData();
    cliente.append('delivery_person_id',userId);
    cliente.append('delivery_person_device',device);
    return this.http.post("http://"+this.urlIP+"/app/update/delivery/device",cliente).map(res => res.json());
  }

  

}
