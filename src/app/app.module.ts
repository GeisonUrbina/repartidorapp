import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {Geolocation} from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import { DecisionPage } from '../pages/decision/decision';
import { GeneralQueryProvider } from '../providers/general-query/general-query';
import { HttpModule } from '@angular/http';
import {MapComponent} from '../components/map/map';
import { EnumProvider } from '../providers/enum/enum';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';
import { Facebook } from '@ionic-native/facebook';
import { IonicStorageModule } from '@ionic/storage';
import { SideMenuPage } from '../pages/side-menu/side-menu';
import { HomePage } from '../pages/home/home';
import {HomePageModule} from '../pages/home/home.module';
import {SideMenuPageModule} from '../pages/side-menu/side-menu.module';
import {LaunchNavigator} from '@ionic-native/launch-navigator';

@NgModule({
  declarations: [
    MyApp,
    MapComponent,
    DecisionPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
    HomePageModule,
    SideMenuPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DecisionPage,
    HomePage,
    SideMenuPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GeneralQueryProvider,
    EnumProvider,
    CallNumber,
    SMS,
    Facebook,
    LaunchNavigator
  ]
})
export class AppModule {}
